# TalkLess
Small Java Extension to make Java talk a little less.



<p>Author: Casey Erdmann</p>
<p>Version 1.0</p>
<p>This API is a very small extension built with the intention of making simple things in Java (such as format and print statements) actually simple. We all know Java loves to talk, with this extension Java will talk just a little less so you can do a little more, alot faster.</p>
 
  <p>TalkLess functions by passing your parameters to the default Java method you intend to call.
  This makes the methods of TalkLess have essentially the exact same core functionality as the original
  methods of Java, because it uses them directly. As such the documentation expressed for this API has many
  references to the official Oracle Java documentation for the original methods.</p>
 
  <p>Each documented item contains an explanation for its literal functionality
  and a reference to Oracle Java documentation for its implied or core functionality.
  
  <p><b>For example:</b></p>
 
  <p>print() literally takes a parameter and passes that parameter to roundPrint.</p>
  <p>roundPrint() literally takes that parameter passed from print() and calls System.out.println()
  with it.</p>
  <p>The implied functionality will be the System.out.println. The literal functionality is how the method
  actually functions.</p>
 
 
  <h1><p><b>INSTRUCTIONS:</b></p></h1>
  <p>This "API" is essentially just an extension of your class.</p>
  <p>Simply download the TalkLess.java class file and import it into your project.</p>
  <p>Once TalkLess is in your project you can then inherit it from any class you would like to use it in.</p>
  <p>In big projects with multiple classes, we recommend inheriting TalkLess as the base layer in your class design.</p>
  <p>This allows you to use TalkLess effectively throughout your project
  with minimal set up.</p>
 
  <b><p>Example:</p></b>
  
  <p>Class: TalkLess</p>
  <p>Class: TestClass extends TalkLess</p>
  <p>Class: Main extends TestClass</p>
 
