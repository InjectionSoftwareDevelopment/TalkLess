package com.company;

import java.io.IOException;
import java.util.Objects;

/**
 * @author Casey Erdmann
 * @version 1.0
 * Created by Casey on 9/25/15.
 * This API is a very small extension built with the intention of making simple things in Java (such as format and print statements)
 * actually simple. We all know Java loves to talk, with this extension Java will talk just a little less
 * so you can do a little more faster, alot faster.
 *
 * <p>TalkLess functions by passing your parameters to the default Java method you intend to call.
 * This makes the methods of TalkLess have essentially the exact same core functionality as the original
 * methods of Java, because it uses them directly. As such the documentation expressed for this API has many
 * references to the official Oracle Java documentation for the original methods.</p>
 *
 * <p>Each documented item contains an explanation for its literal functionality
 * and a reference to Oracle Java documentation for its implied or core functionality.
 * For example:
 *
 * print() literally takes a parameter and passes that parameter to roundPrint.
 * roundPrint() literally takes that parameter passed from print() and calls System.out.println()
 * with it.
 * The implied functionality will be the System.out.println. The literal functionality is how the method
 * actually functions.</p>
 *
 *
 * <h1><p>INSTRUCTIONS:</p></h1>
 * <p>This "API" is essentially just an extension of your class.
 * Simply download the TalkLess.java class file and import it into your project.
 * Once TalkLess is in your project you can then inherit it from any class you would like to use it in.
 * In big projects with multiple classes, we recommend inheriting TalkLess as the base layer in your class design.
 * This allows you to use TalkLess effectively throughout your project
 * with minimal set up.</p>
 *
 * <p>Example:
 *
 * Class: TalkLess
 * Class: TestClass extends TalkLess
 * Class: Main extends TestClass
 * </p>
 */


/**
 * TalkLess class. This class contains methods that call default System.out.* and System.in.* methods. The purpose of these methods is to make
 * writing the default System.out.* and System.in.* methods simple.
 * @param <T> Generic, value used to pass to some methods that need varying types of data.
 */
public class TalkLess<T> {


    /////////////////////////////System.out/////////////////////////

    /**
     * data object
     * Used to make the "round about" calls to the core methods.
     * This allows us to make our methods static, which is necessary
     * for the core design of this API.
     */

    public static TalkLess data;



    /**
     * Takes the parameter the user passes in as a type of Object and
     * calls the roundPrint() method passing the Object t to it.
     * @param t Object, contains the information passed to this method.
     */

    public static void print(Object t){
        if(data == null){
            data = new TalkLess();
        }
        data.roundPrint(t);
    }

    /**
     * Takes the parameter passed from the print() method as a Generic type
     * and calls the System.out.println() method. Using a Generic allows for any
     * compatible data type to be passed in removing the need to explicitly define the
     * type of each parameter.
     * @param t Generic, this is the final data to be printed out.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/PrintWriter.html#println--">println</a>
     */

   public void roundPrint(T t){
       System.out.println(t);

   }


    /**
     * Takes the parameter(s) the user passes in as a type of Object and
     * calls the roundPrintF() method passing Object t and Object f to it.
     * Object t is meant to be a String, while Object f is whatever variables you wish to use
     * in the format of your output.
     * @param t Object, contains the information inteded for a string passed to this method.
     * @param f Object, contains the information inteded for a variable passed to this method.
     */

    public static void printf(Object t, Object f){
        if(data == null){
            data = new TalkLess();
        }
        data.roundPrintF(t, f);


    }

    /**
     * Takes the parameter(s) passed from the printf() method as a Generic type
     * and calls the System.out.printf() method. Using a Generic allows for any
     * compatible data type to be passed in removing the need to explicitly define the
     * type of each parameter. In the case of the System.out.prinf() method, the
     * first parameter must be a string, as a result the String value of t is passed in
     * rather than just the plain Generic of t.
     * @param t Generic, this is the final data to be printed out. This is converted to a string.
     * @param f Generic, this is the final data to be printed out. This is the variable passed in to be formatted into the string.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/PrintWriter.html#printf-java.lang.String-java.lang.Object...-">printf</a>
     */

    public void roundPrintF(T t, T f){

        System.out.printf(String.valueOf(t),f);


    }


    /**
     * Takes the parameter(s) the user passes in as a type of Object and
     * calls the roundFormat() method passing Object t and Object f to it.
     * Object t is meant to be a String, while Object f is whatever variables you wish to use
     * in the format of your output.
     * @param t Object, contains the information inteded for a string passed to this method.
     * @param f Object, contains the information inteded for a variable passed to this method.
     */

    public static void format(Object t, Object f){

        if(data == null){
            data = new TalkLess();
        }
        data.roundFormat(t, f);

    }

    /**
     * Takes the parameter(s) passed from the format() method as a Generic type
     * and calls the System.out.format() method. Using a Generic allows for any
     * compatible data type to be passed in removing the need to explicitly define the
     * type of each parameter. In the case of the System.out.format() method, the
     * first parameter must be a string, as a result the String value of t is passed in
     * rather than just the plain Generic of t.
     * @param t Generic, this is the final data to be printed out. This is converted to a string.
     * @param f Generic, this is the final data to be printed out. This is the variable passed in to be formatted into the string.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/PrintStream.html#format-java.util.Locale-java.lang.String-java.lang.Object...-">printf</a>
     */


    public void roundFormat(T t, T f){

        System.out.format(String.valueOf(t), f);
    }

    /**
     * Takes the parameter the user passes in as a type of Object and
     * calls the roundAppend() method passing Object t to it.
     *
     * @param t Object, this the object that is passed to the roundAppend() method.
     */

    public static void append(Object t){

        if(data == null){
            data = new TalkLess();

        }
        data.roundAppend(t);

    }

    /**
     * Takes the parameters the user passes in as a type of Object, int, and int.
     * Calls the roundAppend() method passing Object t, int start, and int end to it.
     * This version of the append method takes in two extra parameters used to append
     * characters in the Character Sequence (CharSequence).
     * @param t Object, this the object that is passed to the roundAppend() method.
     * @param start int, this is the start(first) character in the CharSequence
     * @param end int, this is the end(last) character in the CharSequence
     */

    public static void append(Object t, int start, int end){


        if (data == null){
            data = new TalkLess();
        }

        data.roundAppend(t, start, end);
    }
    /**
     * Takes the parameter passed from the append() method as a Generic
     * and calls the System.out.append() method but converts the Generic t
     * to its string value for the final output. This allows you to pass anything
     * to be appended but sanitizes the output so it will not have errors.
     *
     * This can be used to append multiple objects together, but will not interfere with
     * the normal .append in java when used with the dot separator. This allows you to get the same
     * functionality out of the default append while being able to access this simple append() method interchangeably.
     *
     * @param t Generic, this is the item that will be appended.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/PrintWriter.html#append-char-">append</a>
     */
    public void roundAppend(T t){

        System.out.append(String.valueOf(t));

    }

    /**
     * Takes the parameter passed from the append() method as a Generic
     * and calls the System.out.append() method but converts the Generic t
     * to its string value for the final output. This allows you to pass anything
     * to be appended but sanitizes the output so it will not have errors.
     *
     * This can be used to append multiple objects together, but will not interfere with
     * the normal .append in java when used with the dot separator. This allows you to get the same
     * functionality out of the default append while being able to access this simple append() method interchangeably.
     *
     * This version of the append method takes in two extra parameters used to append
     * characters in the Character Sequence (CharSequence).
     *
     * @param t Generic, this is the item that will be appended.
     * @param start int, this is the start(first) character in the CharSequence
     * @param end int, this is the end(last) character in the CharSequence
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/PrintWriter.html#append-java.lang.CharSequence-int-int-">append</a>
     */


    public void roundAppend(T t, int start, int end){

        System.out.append(String.valueOf(t), start, end);

    }

    /**
     * This method calls the System.out.close() method. Very simple, the only
     * functionality this has is to close the Print Stream.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/PrintWriter.html#close--">close</a>
     */

    public static void close(){


        System.out.close();

    }

    /**
     * This method calls the System.out.flush() method. Very simple, the only
     * functionality this has is to flush the Print Stream.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/PrintWriter.html#flush--">flush</a>
     */


    public static void flush(){

        System.out.flush();

    }

    /**
     * This method returns the boolean value produced by the System.out.checkError() method.
     * @return System.out.checkError() Boolean, returns true if there is an error with the
     * Print Stream, and false if there is not.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/PrintWriter.html#checkError--">checkError</a>
     */


    public static boolean checkError(){


        return System.out.checkError();


    }


    /**
     * Replication of the System.out.write() method. This tries to write a byte or bytes, but
     * throws an exception otherwise.
     * @param b Byte (array), Bytes to be written.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/FilterOutputStream.html#write-byte:A-">write</a>
     */

   public static void write(byte[] b){

       try {
           System.out.write(b);
       } catch (IOException e) {
           e.printStackTrace();
       }


   }


    /**
     * Replication of the System.out.write() method. This writes an integer.
     * @param b Int, Integer to be written.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/PrintStream.html#write-int-">write</a>
     */

    public static void write(int b){

        System.out.write(b);


    }

    /**
     * Replication of the System.out.write() method. "Writes len bytes from the specified byte array starting at offset off to this output stream".
     * @param buf Byte (array), Byte(s) to be written
     * @param off Int, Integer offset start
     * @param len Int, Integer number of bytes to write
     * @see <a href="http://docs.oracle.com/javase/8/docs/api/java/io/OutputStream.html#write-byte:A-int-int-">write</a>
     */
    public static void write(byte[] buf, int off, int len){

        System.out.write(buf ,off, len);


    }

/////////////////////////////System.in/////////////////////////


    /**
     * This method takes an integer parameter that is passed to the
     * System.in.mark() method.
     * @param readLimit Int, Integer for the maximum limit of bytes that can be read before the mark position becomes invalid.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html#mark-int-">mark</a>
     */

    public static void mark(int readLimit){

        System.in.mark(readLimit);


    }


    /**
     * This method reads an integer, passes it to the System.in.read() method
     * and returns that value.
     * @return System.in.read() Int, Integer the next byte of data, or -1 if the end of the stream is reached.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html#read--">read</a>
     */

    public static int read(){

        try {
            return System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0;


    }

    /**
     * This method reads a byte array value, passes it to the System.in.read() method
     * and returns that value.
     * @param b Byte (array). The value to be passed to System.in.read()
     * @return System.in.read(b) Byte, the total number of bytes read into the buffer, or -1 if there is no more data because the end of the stream has been reached.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html#read-byte:A-">read</a>
     */

    public static byte read(byte[] b){

        try {
            return (byte) System.in.read(b);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0;


    }

    /**
     * This method reads a byte array value, an integer offset, and integer lendth value,
     * and passes it to the System.in.read() method and returns that value.
     * @param b Byte (array). The value to be passed to System.in.read()
     * @param off Int, Integer offset start
     * @param len Int, Integer number of bytes to write
     * @return System.in.read() Byte, the total number of bytes read into the buffer, or -1 if there is no more data because the end of the stream has been reached.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html#read-byte:A-int-int-">read</a>
     */

    public static byte read(byte[] b, int off, int len) {



        try {
            return (byte) System.in.read(b, off, len);

        } catch (IOException e) {
            e.printStackTrace();
        }


        return 0;

    }


    /**
     * Calls System.in.reset. "The method reset for class InputStream does nothing except throw an IOException."
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html#reset--">reset</a>
     */

    public static void reset(){

        try {
            System.in.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Takes a parameter of type Long, and passes it to the System.in.skip().
     * @param n Long, the number of bytes to be skipped.
     * @return System.in.skip(n) Long, the actual number of bytes skipped.
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html#skip-long-">skip</a>
     */

    public static long skip(long n){

        try {
            return System.in.skip(n);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0;

    }


}
